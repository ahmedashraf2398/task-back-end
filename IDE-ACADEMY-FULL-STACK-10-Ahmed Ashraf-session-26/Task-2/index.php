<?php
require_once 'pages/circle.php';
require_once 'pages/shape.php';
require_once 'pages/rectangle.php';

switch ($_GET['p']) {
    case 'circle':
        require_once 'pages/circle.php';
        break;
    case 'rectangle':
        require_once 'pages/rectangle.php';
        break;
    case 'shape':
        require_once 'pages/shape.php';
        break;
}