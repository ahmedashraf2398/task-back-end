<?php
require_once 'shape.php';

class circle extends shape {
    const shape_type = 3 ;

    protected $radius;
    public    $name;
    private   $id;


    public function __construct($radius) {
        parent::shape(1.2);
        $this->id = uniqid();
        $this->radius = $radius;
    }
    
    function circle_area() {
        $area = 3.14 * $this->radius ;
        return $area;
    }
    function  getFullDescription() {
        echo 'Shape<' .$this->id . '>: ' . $this->name . ' - ' . $this->radius;
    }
}

?>