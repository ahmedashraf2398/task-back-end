<?php
    include  'action.php';
 ?>
<?php require_once 'header.php'; ?>

    <div class="container">
    <div class="row justify-content-center">
    <div class="col-md-10">
    <h3 class="text-center text-dark p-2">Crud App</h3>
    <hr>
    <?php if(isset($_SESSION['response'])){ ?>
    <div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible text-center">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <?= $_SESSION['response']; ?>
</div>  
    <?php } unset( $_SESSION[' response ']); ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3">
    <h3 class="text-center text-info">Add Record</h3>
    <form action="action.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?=$id; ?> " >
    <div class="form-group">
    <input type="text" name="name" value="<?=$name; ?>" class="form-control" placeholder="Enter your name" required>
    </div>
    <div class="form-group">
    <input type="email" name="email" value="<?=$email; ?>" class="form-control" placeholder="Enter your email" required>
    </div>
    <div class="form-group">
    <input type="tel" name="phone" value="<?=$phone; ?>" class="form-control" placeholder="Enter your phone" required>
    </div>
    <div class="form-group">
    <input type="hidden" name="oldimage" value="<?=$photo; ?> " >
    <input type="file" name="image" value="<?=$name; ?> " class="custom-file">
    <img src="<?=$photo;?>" width="50" class="img-thumbnail">
    </div>
    <div class="form-group">
    <?php if($update==true){ ?>
      <input type="submit" name="update" class="btn btn-success btn-block" value="update Record">
    <?php } else{ ?>
    <input type="submit" name="add" class="btn btn-primary btn-block" value="Add Record">
    <?php } ?>
    </div>
    </form>
    </div>
    <div class="col-md-9">
    <?php
      $query="SELECT * FROM crud";
      $stmt=$conn->prepare($query);
      $stmt->execute();
      $result=$stmt->get_result()
     ?>
    <h3 class="text-center text-info">Record present in database</h3>
    <table class="table table-bordered table-hover">
    <thead>
      <tr>
        <th>#</th>
        <th>Image</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php while($row=$result->fetch_assoc()){ ?>
      <tr>
        <td> <?= $row['id']; ?> </td>
        <td> <img src=" <?= $row['photo']; ?>" width="50px"></td>
        <td> <?= $row['name']; ?></td>
        <td> <?= $row['email']; ?></td>   
        <td> <?= $row['phone']; ?></td>
        <td>
        <a href="details.php?details=<?=  $row['id']; ?>" class="badge badge-primary p-2">Details</a> |
        <a href="action.php?delete=<?= $row['id']; ?>" class="badge badge-danger p-2">Delete</a> |
        <a href="index.php?edit=<?= $row['id']; ?>" class="badge badge-success p-2">Edit</a>
        </td>
      </tr>
    <?php } ?>
     
    </tbody>
  </table>
    </div>
    </div>
    </div>


<?php require_once 'footer.php'; ?>

